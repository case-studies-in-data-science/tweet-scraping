import GetOldTweets3 as got
import csv
from datetime import datetime
import time

now = datetime.now().strftime("%Y%m%d_%H%M%S")  # for output file name

days_in_month = {
    # "2": 29,
    # "3": 31,
    # "4": 30,
    "5": 31,
    "6": 30,
    "7": 31,
    "8": 31,
    "9": 16 
}

text_query = "Coronavirus Victoria"
count = 0  # All possible tweets

out_file = open(
    "out/tweets " + now + " " + text_query + ".csv",
    mode="w",
    newline="",
    encoding="utf-8-sig",
)

csv_writer = csv.writer(
    out_file, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL
)

csv_writer.writerow(
    [
        "id",
        "permalink",
        "username",
        "to",
        "text",
        "date",
        "retweets",
        "favorites",
        "mentions",
        "hashtags",
        "geo",
    ]
)

for month in days_in_month:
    for day in range(days_in_month[month]):
        since_date = "2020-0" + month + "-" + str(day + 1)

        time.sleep(1) # to prevent exceeding frequency limit
        print(since_date)

        if day + 1 == days_in_month[month]:
            until_date = "2020-0" + str(int(month) + 1) + "-01"
        else:
            until_date = "2020-0" + month + "-" + str(day + 2)

        tweetCriteria = (
            got.manager.TweetCriteria()
            .setQuerySearch(text_query)
            .setSince(since_date)
            .setUntil(until_date)
            .setMaxTweets(count)
        )  # Creation of list that contains all tweets

        tweets = got.manager.TweetManager.getTweets(
            tweetCriteria
        )  # Creating list of chosen tweet data

        # Write headers

        for tweet in tweets:
            data_to_write = [
                str(tweet.id),
                tweet.permalink,
                tweet.username,
                tweet.to,
                tweet.text,
                tweet.date,
                tweet.retweets,
                tweet.favorites,
                tweet.mentions,
                tweet.hashtags,
                tweet.geo,
            ]

            csv_writer.writerow(data_to_write)


out_file.close()
